﻿using UnityEngine;
using System.Collections;

public class JM_PicnicScript : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Bug")
        {
            Debug.Log("Should be dying");
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
    }
}
