﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JM_EnemyPathing : MonoBehaviour
{
    public List<GameObject> Path;

    public GameObject Target;

    public float speed;
    //public GameObject OldTarget;

    public int waypoint;

	// Use this for initialization

    void Awake()
    {
        FindWayPoints();
    }
	void Start ()
    {
        waypoint = 0;
        speed = 4f;
        Target = Path[waypoint];
    }
	
	// Update is called once per frame
	void Update ()
    {
        Movement();
    }

    void Movement()
    {
        Debug.Log("Should be moving");
        transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, speed * Time.deltaTime);

        float dis;
        dis = Vector3.Distance(transform.position, Target.transform.position);

        if(dis < 0.025f)
        {
            if(waypoint < Path.Count)
            {
                Target = Path[waypoint++];
            }
            else
            {
                //nothing
            }
        }
    }

    void FindWayPoints()
    {
        Path[0] = GameObject.Find("Waypoint");
        Path[1] = GameObject.Find("Waypoint1");
        Path[2] = GameObject.Find("Waypoint2");
        Path[3] = GameObject.Find("Waypoint3");
        Path[4] = GameObject.Find("Waypoint4");
        Path[5] = GameObject.Find("Waypoint5");
        Path[6] = GameObject.Find("Waypoint6");
    }
}
