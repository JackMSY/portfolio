﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class JM_TowerScript : MonoBehaviour
{
    public float FieldOfViewAngle = 110f;
    public RaycastHit hit;
    public SphereCollider Sphere;

    public bool Pressed;
    public Animator anim;

    // PowerUp
    public Image powerUp;

	// Use this for initialization
	void Start ()
    {
        Sphere = gameObject.GetComponent<SphereCollider>();
        anim = GetComponentInChildren <Animator>();
    }


    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "Bug")
        {
            Debug.Log("Bug in my area");
            Vector3 direction = col.transform.position - transform.position;

            float angle = Vector3.Angle(transform.position, col.transform.position);

            if (angle < FieldOfViewAngle)
            {
                if (Physics.Raycast(transform.position + new Vector3 (0, 0.5f ,0), direction.normalized, out hit, Sphere.radius))
                {
                    if (hit.collider.gameObject.tag == "Bug" && Pressed)
                    {
                        Debug.Log("I've hit him");
                        powerUp.fillAmount += 10/100f;

                        Destroy(hit.collider.gameObject);

                    }
                }
            }
        }
    }

    void OnMouseDown()
    {
        anim.SetTrigger("Spray");

        Pressed = true;
    }

    void OnMouseUp()
    {
        Pressed = false;
    }
}
